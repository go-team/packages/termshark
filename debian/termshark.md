termshark(1) -- A wireshark-inspired terminal user interface for tshark
=============================================

## SYNOPSIS

`termshark` [options] [FilterOrFile]

## DESCRIPTION

A wireshark-inspired terminal user interface for tshark.
Analyze network traffic interactively from your terminal.
See https://github.com/gcla/termshark for more information.

## OPTIONS

 * `-i`=<interface> :
  Interface to read.

 * `-r`=<file> :
  Pcap file to read.

 * `-d`=<layer_type>==<selector>,<decode-as protocol> :
  Specify dissection of layer type.

 * `-Y`=<displaY_filter> :
  Apply display filter.

 * `-f`=<capture_filter> :
  Apply capture filter.

 * `--pass-thru`=[yes|no|auto|true|false] :
  Run tshark instead (auto => if stdout is not a tty). (default: auto)

 * `--log-tty`=[yes|no|true|false] :
  Log to the terminal.. (default: false)

 * `-h`, `--help` :
  Show this help message.

 * `-v`, `--version` :
  Show version information.

## ARGUMENTS

`FilterOrFile`

    Filter (capture for iface, display for pcap), or pcap file to read.

## SEE ALSO

tshark(1)